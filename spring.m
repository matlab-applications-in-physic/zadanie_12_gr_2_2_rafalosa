% http://www.mif.pg.gda.pl/pl/download/LabFizNowe/rozpakowane/Cwicz63_01.pdf

% https://www.engineeringtoolbox.com/modulus-rigidity-d_946.html I've 
% chosen rigidity modulus for structural steel

% https://platforma.polsl.pl/rif/pluginfile.php/14722/mod_resource/content/1/Drgania_harmoniczne_2019.pdf

% Rafał Osadnik
% Engineering Physics
% Institute of Physics

clear

RigidityModulus = 79e9; %Rigidity modulus in [Pa]

RadiusOfWire = 0.002; %Radius of spring wire in [m]

RadiusOfSpring = 2 * 0.0254; %Radius of spring [m]

NumberOfTurns = 2:1:50; 

AccelerationOfActingForce = 9.81; %Earth's gravitational acceleration

ObjectMass = 0.01; %Mass of the object in [kg]

F = ObjectMass * AccelerationOfActingForce; %Newton's second law

Tau = 2;

Beta = 1/(2*Tau);

alpha_0 = F/ObjectMass; 

FrequencyDomain = (65:0.01:380); %Creating the frequency domain for simulation

for i = 1:size(NumberOfTurns,2)

SpringConstant = RigidityModulus * RadiusOfWire^4/(RadiusOfSpring^3 * NumberOfTurns(i) * 4);

ResonantFrequency = sqrt(SpringConstant/ObjectMass);
 
    for j = 1:size(FrequencyDomain,2) % For every frequency calculate the responding amplitude
    
        Amplitudes(j,i) = alpha_0/sqrt((ResonantFrequency^2 - FrequencyDomain(j)^2)^2 + 4*Beta^2*FrequencyDomain(j)^2);
        
    end
    [PeakAmplitudes(i),index] = max(Amplitudes(:,i)); %Searching for the max amplitude for every number of turns and tracking it's index
    ResonantFrequencies(i) = FrequencyDomain(index)/2/pi;
    
    subplot(2,1,1);
    plot(FrequencyDomain/2/pi,Amplitudes) %Plotting data
   
end
xlim([10 60])
xlabel('Oscillation frequency [Hz]'); %Labeling the axes
ylabel('Amplitude [m]')

 subplot(2,1,2);
 plot(NumberOfTurns,PeakAmplitudes)
 
 
xlabel('Number of turns');
ylabel('Maximum amplitude [m]')
xlim([2 50]);
 
saveas(gcf,'resonance.pdf'); % Saving figure into a pdf

FileID = fopen('resonance_analysis_results.dat','w'); % Results of the simulation are written into a file

fprintf(FileID,'r = %f [m];R = %f [m];m = %f [kg];Tau = %f [s];g = %f [m/s^2];G = %.2d [Pa] \n \n',RadiusOfWire,RadiusOfSpring,ObjectMass,Tau,AccelerationOfActingForce,RigidityModulus); %Printing header

for i = 1:size(NumberOfTurns,2)
    
fprintf(FileID,'N = %.0f;A = %.3f [m]; f = %.3f [Hz]\n',NumberOfTurns(i),PeakAmplitudes(i),ResonantFrequencies(i));

end

fclose(FileID);


